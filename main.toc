\changetocdepth {4}
\babel@toc {brazil}{}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdução}}{7}{chapter.1}%
\contentsline {section}{\numberline {1.1}O PESQUISADOR E SEU CONTEXTO}{7}{section.1.1}%
\contentsline {section}{\numberline {1.2}APRESENTANDO A PESQUISA}{7}{section.1.2}%
\contentsline {section}{\numberline {1.3}PROBLEMA DE PESQUISA}{7}{section.1.3}%
\contentsline {section}{\numberline {1.4}JUSTIFICATIVA}{7}{section.1.4}%
\contentsline {section}{\numberline {1.5}HIPÓTESES}{7}{section.1.5}%
\contentsline {section}{\numberline {1.6}OBJETIVOS}{7}{section.1.6}%
\contentsline {subsection}{\numberline {1.6.1}Objetivo Geral}{7}{subsection.1.6.1}%
\contentsline {subsection}{\numberline {1.6.2}Objetivos Específico}{7}{subsection.1.6.2}%
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {REFERENCIAL TEÓRICO}}{8}{chapter.2}%
\contentsline {section}{\numberline {2.1}TENDÊNCIA PEDAGÓGICA}{8}{section.2.1}%
\contentsline {section}{\numberline {2.2}TEORIA DE APRENDIZAGEM}{8}{section.2.2}%
\contentsline {section}{\numberline {2.3}PRÁTICA PEDAGÓGICA DA PESQUISA}{8}{section.2.3}%
\contentsline {section}{\numberline {2.4}CONTEÚDO A SER TRABALHADO NA PESQUISA}{8}{section.2.4}%
\contentsline {section}{\numberline {2.5}TECNOLOGIA EDUCACIONAL UTILIZADA NA PESQUISA}{8}{section.2.5}%
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {INTERVENÇÃO PEDAGÓGICA}}{9}{chapter.3}%
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {REVISÃO DE LITERATURA}}{10}{chapter.4}%
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {METODOLOGIA}}{11}{chapter.5}%
\contentsline {section}{\numberline {5.1}LOCUS E SUJEITOS DA PESQUISA}{11}{section.5.1}%
\contentsline {section}{\numberline {5.2}METODOLOGIA DA PESQUISA}{11}{section.5.2}%
\contentsline {section}{\numberline {5.3}INSTRUMENTOS DE COLETA E PRODUÇÃO DE DADOS}{11}{section.5.3}%
\contentsline {section}{\numberline {5.4}METODOLOGIAS DE ANÁLISE DE DADOS}{11}{section.5.4}%
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {DISCUSSÃO DOS DADOS}}{12}{chapter.6}%
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {7}\MakeTextUppercase {CONSIDERAÇÕES FINAIS}}{13}{chapter.7}%
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\MakeTextUppercase {Refer\^encias}}{14}{section*.5}%
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
